<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryaIlmiahHasPenulisTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'karya_ilmiah_has_penulis';

    /**
     * Run the migrations.
     * @table karya_ilmiah_has_penulis
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('karya_ilmiah_id');
            $table->unsignedInteger('penulis_id');

            $table->index(["penulis_id"], 'fk_karya_ilmiah_has_penulis_penulis1_idx');

            $table->index(["karya_ilmiah_id"], 'fk_karya_ilmiah_has_penulis_karya_ilmiah1_idx');


            $table->foreign('karya_ilmiah_id', 'fk_karya_ilmiah_has_penulis_karya_ilmiah1_idx')
                ->references('id')->on('karya_ilmiah')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('penulis_id', 'fk_karya_ilmiah_has_penulis_penulis1_idx')
                ->references('id')->on('penulis')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
