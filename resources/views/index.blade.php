@extends('include.app')

@section('content')


      <section class="section section-variant-2 bg-default novi-background bg-cover text-center">
        <div class="container container-wide">
          <div class="isotope-wrap row row-fix row-50">
            <!-- Isotope Filters-->
            <div class="col-xl-12">
              <ul class="isotope-filters isotope-filters-horizontal">
                <li class="block-top-level">
                  <p class="big">Choose your category:</p>
                  <!-- Isotope Filters-->
                  <button class="isotope-filters-toggle button button-xs button-primary" data-custom-toggle="#isotope-filters-list-1" data-custom-toggle-hide-on-blur="true">Filter<span class="caret"></span></button>
                  <ul class="isotope-filters-list isotope-filters-list-buttons" id="isotope-filters-list-1">
                    <li><a class="button-xs active" data-isotope-filter="*" data-isotope-group="movies" href="#">All Categories</a></li>

                    @foreach (App\Pembahasan::paginate(5) as $item)
                    
                    @if(!empty($item->nama))
                    <li><a class="button-xs" data-isotope-filter="type {{$item->id}}" data-isotope-group="movies" href="#">{{$item->nama}}</a></li>
          @endif
                    @endforeach
                 
                  </ul>
                </li>
              </ul>
            </div>
            <!-- Isotope Content-->


          
            <div class="col-xl-12">
              <div class="isotope isotope-md row row-30" data-isotope-layout="fitRows" data-isotope-group="movies" data-lightgallery="group">
               
               
                @foreach (App\KaryaIlmiah::paginate(40) as $item)

                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type {{$item->pembahasan->first()->id}}">
                  <div class="product">
                    <div class="product-image"><a href="/{{$item->uid}}">
                      {{-- @php

                      
                 
                            $im = new imagick('https://en.wikipedia.org/wiki/JPEG#/media/File:Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png');
                            $im->setImageFormat('jpg');
                            header('Content-Type: image/jpeg');
                            echo $im;
                            ?>
                      @endphp --}}


                          {{-- <img src='data:image/jpg;base64,".base64_encode({{$im}})."' /> --}}
          

                        {{-- <img data-pdf-thumbnail-file="{{$item->link}}.pdf" data-pdf-thumbnail-width="188" data-pdf-thumbnail-height="246" > --}}
                        <canvas id="the-canvas-{{$item->id}}" style=" direction: ltr;" width="188" height="246"></canvas>

                    </a></div>
                    <div class="product-title">
                      <h5><a href="/{{$item->uid}}"> {{$item->judul}}
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="/{{$item->uid}}">Read</a></div>
                  </div>
                </div>


                    
                @endforeach
          
              
              </div>
            </div>
          </div><a class="button button-primary button-nina" href="shop-3-columns-layout.html">view more</a>
        </div>
      </section>

      <!-- Small Features-->
      
  
      @endsection
      <!-- Page Footer-->
      <!-- Footer Default-->
     

      @push('scripts')

        {{-- <script src="{{asset('/js/pdf_thumbnail.js')}}"></script> --}}
        {{-- <script src="{{asset('/js/pdf.js')}}"></script> --}}
 


<script src="{{asset('js/pdf.js')}}"></script>



<script id="script">
  //
  // If absolute URL from the remote server is provided, configure the CORS
  // header on that server.
  //
pdfjsLib.GlobalWorkerOptions.workerSrc = "{{asset('/js/pdf.worker.js')}}";


  //
  // The workerSrc property shall be specified.
  //

  //
  // Asynchronous download PDF
  //
  
  @foreach (App\KaryaIlmiah::paginate(40) as $karya)
  var proxyurl = "https://cors-anywhere.herokuapp.com/";
  var url = '{{$karya->link}}.pdf';

  var loadingTask = pdfjsLib.getDocument(proxyurl+url);

  loadingTask.promise.then(function(pdf) {
    //
    // Fetch the first page
    //
    pdf.getPage(1).then(function(page) {
      var scale = 0.3;
      var viewport = page.getViewport({ scale: scale, });

      //
      // Prepare canvas using PDF page dimensions
      //
      var canvas = document.getElementById('the-canvas-{{$karya->id}}');
      var context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      //
      // Render PDF page into canvas context
      //
      var renderContext = {
        canvasContext: context,
        viewport: viewport,
      };
      page.render(renderContext);
    });
  });


  @endforeach
  
</script>

     
        
      @endpush
  
 