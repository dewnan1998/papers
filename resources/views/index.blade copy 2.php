@extends('include.app')

@section('content')


      <section class="section section-variant-2 bg-default novi-background bg-cover text-center">
        <div class="container container-wide">
          <div class="isotope-wrap row row-fix row-50">
            <!-- Isotope Filters-->
            <div class="col-xl-12">
              <ul class="isotope-filters isotope-filters-horizontal">
                <li class="block-top-level">
                  <p class="big">Choose your category:</p>
                  <!-- Isotope Filters-->
                  <button class="isotope-filters-toggle button button-xs button-primary" data-custom-toggle="#isotope-filters-list-1" data-custom-toggle-hide-on-blur="true">Filter<span class="caret"></span></button>
                  <ul class="isotope-filters-list isotope-filters-list-buttons" id="isotope-filters-list-1">
                    <li><a class="button-xs active" data-isotope-filter="*" data-isotope-group="movies" href="#">All Categories</a></li>

                    @foreach (App\Pembahasan::paginate(5) as $item)
                    
                    @if(!empty($item->nama))
                    <li><a class="button-xs" data-isotope-filter="type {{$item->id}}" data-isotope-group="movies" href="#">{{$item->nama}}</a></li>
          @endif
                    @endforeach
                 
                  </ul>
                </li>
              </ul>
            </div>
            <!-- Isotope Content-->


          
            <div class="col-xl-12">
              <div class="isotope isotope-md row row-30" data-isotope-layout="fitRows" data-isotope-group="movies" data-lightgallery="group">
               
               
                @foreach (App\KaryaIlmiah::paginate(40) as $item)

                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type {{$item->pembahasan->first()->id}}">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html">
                      {{-- @php

                      
                 
                            $im = new imagick('https://en.wikipedia.org/wiki/JPEG#/media/File:Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png');
                            $im->setImageFormat('jpg');
                            header('Content-Type: image/jpeg');
                            echo $im;
                            ?>
                      @endphp --}}


                          {{-- <img src='data:image/jpg;base64,".base64_encode({{$im}})."' /> --}}
          

                        {{-- <img data-pdf-thumbnail-file="{{$item->link}}.pdf" data-pdf-thumbnail-width="188" data-pdf-thumbnail-height="246" > --}}

                    </a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html"> {{$item->judul}}
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">Read</a></div>
                  </div>
                </div>


                    
                @endforeach
          
              
              </div>
            </div>
          </div><a class="button button-primary button-nina" href="shop-3-columns-layout.html">view more</a>
        </div>
      </section>

      <!-- Small Features-->
      
  
      @endsection
      <!-- Page Footer-->
      <!-- Footer Default-->
     

      @push('scripts')

        {{-- <script src="{{asset('/js/pdf_thumbnail.js')}}"></script> --}}
        {{-- <script src="{{asset('/js/pdf.js')}}"></script> --}}
        <script src="//npmcdn.com/pdfjs-dist/build/pdf.js"></script>

        <script>
 

      pdfjsLib.GlobalWorkerOptions.workerSrc = "{{asset('/js/pdf.worker.js')}}";

      function makeThumb(page) {
  // draw page to fit into 96x96 canvas
  var vp = page.getViewport({scale:1});
  var canvas = document.createElement("canvas");
  canvas.width = canvas.height = 96;
  var scale = Math.min(canvas.width / vp.width, canvas.height / vp.height);
  return page.render({canvasContext: canvas.getContext("2d"), viewport: page.getViewport(scale)}).promise.then(function () {
    return canvas;
  });
}

pdfjsLib.getDocument("https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf").promise.then(function (doc) {
  var pages = []; while (pages.length < doc.numPages) pages.push(pages.length + 1);
  return Promise.all(pages.map(function (num) {
    // create a div for each page and build a small canvas for it
    var div = document.createElement("div");
    document.body.appendChild(div);
    return doc.getPage(num).then(makeThumb)
      .then(function (canvas) {
        div.appendChild(canvas);
    });
  }));
}).catch(console.error);
   


        </script>
     
        
      @endpush
  
 