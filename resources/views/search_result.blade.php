@extends('include.app')

@section('content')

<section class="section section-lg bg-default">
    <div class="container container-wide">
        <div class="row row-fix justify-content-center">
            <div class="col-lg-9 col-xxl-6">
    
                <div class="rd-search-results">
                    <div id="search-results">

                        <ol class="search-list">

                            

                        @foreach( $document as $data)
                        <li class="">

                            <h5 class="search-title font-bold"><a target="_top" href="/{{$data->uid}}"
                                    class="">{{$data->judul}}</a></h5>
                            <p> 
                                
                               {{ strlen($data->doc) > 200 ? substr($data->doc,0,200)."..." : $data->doc}}
                            </p>
                        </li>
                        @endforeach
                            


                        </ol>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
<!-- Page Footer-->
<!-- Footer Default-->


@push('scripts')

@endpush