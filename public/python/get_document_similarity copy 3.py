from database import getKaryaIlmiah 
# from preprocessing import tokenize
from  karya_ilmiah import KaryaIlmiah
# import numpy as np
from gensim.models import Word2Vec
from pathlib import Path
import dill
import json

import sys


m4 = Word2Vec.load('word2vec-4.model')




try:
    with open('karya_ilmiah.pkl', 'rb') as f:
        # print ("Load karya ilmiah . . .")
        karya_ilmiah = dill.load(f)

except (OSError, IOError) as e:

    data = getKaryaIlmiah()

    karya_ilmiah = []
    for item in data :

        karya = KaryaIlmiah(item)
    
    # print(get_mean_vector(m4,item['summary']))
        # mean = get_mean_vector(m4,tokenize(item['summary']))
        # karya.setVector(mean)
        karya_ilmiah.append(karya)
    


    # print ("Karya ilmiah diset")

    with open('karya_ilmiah.pkl', 'wb') as f:
        dill.dump(karya_ilmiah, f)





# print ("get rekomendasi")


data_folder = Path("document_similar_1000/")

filename = 'karya_ilmiah-'+str(sys.argv[1])+'.pkl'


file_to_open = data_folder / filename

# print(file_to_open)

import time
start_time = time.time()
temp_time = time.time()

from DocSim import DocSim
ds = DocSim(m4)




try:
    data_folder = Path("document_similar_1000/")

    filename = 'karya_ilmiah-'+str(sys.argv[1])+'.pkl'

    file_to_open = data_folder /  filename

    # print(file_to_open)

    with open(file_to_open, 'rb') as f:
        # print ("Load Rekomendasi . . .")
        sim_result = dill.load(f)
        res = []
        for data in sim_result[0:4]:
            res.append({
                'id' : data['id'],
                'uid' : data['uid'],
                'judul' : data['judul'],
                'doc' : data['doc'],

            })
        python2json = json.dumps(res)
        # sys.exit( str(python2json))
        print( python2json)


except (OSError, IOError) as e:
    # print("Membuat rekomendasi dokumen "+str(sys.argv[1]))

    


    sim_result = ds.calculate_similarity(item, karya_ilmiah[0:1000])

    # print(sim_result[0:5])

    data_folder = Path("document_similar_1000/")

    filename = 'karya_ilmiah-'+str(sys.argv[1])+'.pkl'

    file_to_open = data_folder /  filename

    



    with open(file_to_open, 'wb') as f:
        dill.dump(sim_result, f)


    # print("Dokumen berhasil dibuat")
    # print("--- %s seconds ---" % (time.time() - temp_time))
  

        for data in sim_result[0:4]:
            res.append({
                'id' : data['id'],
                'uid' : data['uid'],
                'judul' : data['judul'],
                'doc' : data['doc'],

            })
        python2json = json.dumps(res)
        # sys.exit( str(python2json))
        print( python2json)

