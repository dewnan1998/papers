<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryaIlmiahTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'karya_ilmiah';

    /**
     * Run the migrations.
     * @table karya_ilmiah
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('judul')->nullable();
            $table->string('link')->nullable();
            $table->text('summary')->nullable();
            $table->date('publish')->nullable();
            $table->string('bahasa', 45)->nullable();
            $table->string('uid', 45)->nullable();

            $table->unique(["id"], 'id_UNIQUE');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
