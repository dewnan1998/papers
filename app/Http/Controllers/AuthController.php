<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
class AuthController extends Controller
{
    //

    public function loginView()
    {

        $session  = Session::get('session');


        $user = User::where('session' , $session)->first();

        // dd($user);

        if(!empty($user))
        {

            Auth::login($user,true);
            return redirect('/');
        }
        return view('login');
    }
    public function login(Request $request)
    {

             $session = Str::random(30);


             Session::put('session', $session);

            $user = User::firstOrCreate([
                'npm' => $request->npm,
            ],
            [
                'nama' =>$request->name,
                'session' => $session,
                'remember_token' => 1,
            ]);

            Auth::login($user,true);
            return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

}
