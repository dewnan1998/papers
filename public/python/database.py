import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="skripsi"
)

def getDataset():
  # Something
    mycursor = mydb.cursor()
    
    mycursor.execute("SELECT summary FROM karya_ilmiah")

    myresult = mycursor.fetchall()

    return myresult

def getKaryaIlmiah():
  # Something
    mycursor = mydb.cursor(dictionary=True)
    
    mycursor.execute("SELECT id,summary,judul,link,uid FROM karya_ilmiah")

    myresult = mycursor.fetchall()

    return myresult



# for x in myresult:
#   print(x)
