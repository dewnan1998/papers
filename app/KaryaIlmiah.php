<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KaryaIlmiah extends Model
{
    //
    protected $table = "karya_ilmiah";
    protected $fillable = [
        'judul', 'summary', 'publish','bahasa','link' ,'uid',
    ];
    
    public $timestamps = false;
    protected $dates = ['created_at'];


    public function penulis ()
    {
        return $this->belongsToMany(Penulis::class,'karya_ilmiah_has_penulis','karya_ilmiah_id','penulis_id');
    }
    

    public function interaksi()
    {
        return $this->hasMany(InteraksiUserKaryaIlmiah::class ,'karya_ilmiah_id');
    }


    public function pembahasan ()
    {
        return $this->belongsToMany(Pembahasan::class,'karya_ilmiah_has_pembahasan','karya_ilmiah_id','pembahasan_id');
    }
    


}
