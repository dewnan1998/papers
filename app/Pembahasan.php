<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembahasan extends Model
{
    //

    protected $table = "pembahasan";

    protected $fillable = [
        'nama', 'subject_code'
    ];

    public $timestamps = false;




    public  function createPembahasanFromArxivSubject()
    {

        $param = $this->subject_code;
        $x = "";
        if ("AI"  == $param) $x = " Artificial Intelligence";
        if ("CC"  == $param) $x = " Computational Complexity";
        if ("CG"  == $param) $x = " Computational Geometry";
        if ("CE"  == $param) $x = " Computational Engineering";
        if ("CL"  == $param) $x = " Computation and Language";
        if ("CV"  == $param) $x = " Computer Vision and Pattern Recognition";
        if ("CY"  == $param) $x = " Computers and Society";
        if ("CR"  == $param) $x = " Cryptography and Security";
        if ("DB"  == $param) $x = " Databases";
        if ("DS"  == $param) $x = " Data Structures and Algorithms";
        if ("DL"  == $param) $x = " Digital Libraries";
        if ("DM"  == $param) $x = " Discrete Mathematics";
        if ("DC"  == $param) $x = " Distributed and Cluster Computing";
        if ("ET"  == $param) $x = " Emerging Technologies";
        if ("FL"  == $param) $x = " Formal Languages and Automata Theory";
        if ("GL"  == $param) $x = " General Literature";
        if ("GR"  == $param) $x = " Graphics";
        if ("AR"  == $param) $x = " Hardware Architecture";
        if ("HC"  == $param) $x = " Human Computer Interaction";
        if ("IR"  == $param) $x = " Information Retrieval";
        if ("IT"  == $param) $x = " Information Theory";
        if ("LG"  == $param) $x = " Machine Learning";
        if ("LO"  == $param) $x = " Logic in Computer Science";
        if ("MS"  == $param) $x = " Mathematical Software ";
        if ("MA"  == $param) $x = " Multiagent Systems ";
        if ("MM"  == $param) $x = " Multimedia";
        if ("NI"  == $param) $x = " Networking and Internet Architecture ";
        if ("NE"  == $param) $x = " Neural and Evolutionary Computation ";
        if ("NA"  == $param) $x = " Numerical Analysis ";
        if ("OS"  == $param) $x = " Operating Systems ";
        if ("OH"  == $param) $x = " Other ";
        if ("PF"  == $param) $x = " Performance";
        if ("SI"  == $param) $x = " Social and Information Networks ";
        if ("SE"  == $param) $x = " Software Engineering";
        if ("SD"  == $param) $x = " Sound";
        if ("SC"  == $param) $x = " Symbolic Computation";
        if ("SY"  == $param) $x = " Systems and Control";

        $this->nama = $x;
        $this->save();
    }
}
