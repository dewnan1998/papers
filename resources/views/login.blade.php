<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <!-- Site Title-->
    <title> Papers </title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <script src="/cdn-cgi/apps/head/3ts2ksMwXvKRuG480KNifJ2_JNM.js"></script><link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald:200,400%7CLato:300,400,300italic,700%7CMontserrat:900">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fonts.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="https://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>


<div class="section-md text-center">
    <div class="container">
      <h3>Login Session</h3>
      <div class="row row-fix justify-content-sm-center">
        <div class="col-md-8 col-lg-6 col-xl-4">
          <!-- RD Mailform-->
          <form   method="POST" action="/login">
            @csrf
            <div class="form-wrap form-wrap-validation">
              <input class="form-input form-control-has-validation" id="forms-login-name" type="text" name="name" data-constraints="@Required"><span class="form-validation"></span>
              <label class="form-label rd-input-label" for="forms-login-name">Nama</label>
            </div>
            <div class="form-wrap form-wrap-validation">
              <input class="form-input form-control-has-validation" id="forms-login-password" type="text" name="npm" data-constraints="@Required"><span class="form-validation"></span>
              <label class="form-label rd-input-label" for="forms-login-password">Npm</label>
            </div>
            <div class="form-button">
              <button class="button button-block button-primary button-nina" type="submit"><span style="transition: opacity 0.22s ease 0s, transform 0.22s ease 0s, color 0.22s ease 0s;">S</span><span style="transition: opacity 0.22s ease 0.03s, transform 0.22s ease 0.03s, color 0.22s ease 0s;">i</span><span style="transition: opacity 0.22s ease 0.06s, transform 0.22s ease 0.06s, color 0.22s ease 0s;">g</span><span style="transition: opacity 0.22s ease 0.09s, transform 0.22s ease 0.09s, color 0.22s ease 0s;">n</span><span style="transition: opacity 0.22s ease 0.12s, transform 0.22s ease 0.12s, color 0.22s ease 0s;"></span><span style="transition: opacity 0.22s ease 0.15s, transform 0.22s ease 0.15s, color 0.22s ease 0s;">I</span><span style="transition: opacity 0.22s ease 0.18s, transform 0.22s ease 0.18s, color 0.22s ease 0s;">n</span><span class="button-original-content" style="transition: background 0.22s ease 0s, color 0.22s ease 0s, transform 0.22s ease 0.21s;">Sign In</span></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  

  <script src={{asset("js/core.min.js")}}></script>
  <script src={{asset("js/script.js")}}></script>




  </body>
</html>
  <!-- Page Footer-->
