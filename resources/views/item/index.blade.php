@extends('include.app')

@section('content')

<section class="section section-lg bg-default">
  <div class="container container-bigger product-single">
    <div class="row row-fix justify-content-sm-center justify-content-lg-between row-30 align-items-lg-center">
      <div class="col-lg-5 col-xl-6 col-xxl-5">
        <div class="product-single-preview">
          <div class="unit flex-column flex-md-row align-items-md-center unit-spacing-md-midle unit--inverse unit-sm">
            <div class="unit-body">
              {{-- <ul class="product-thumbnails">
                <li class="active" data-large-image="images/shop-01-420x550.png"><img src="images/shop-01-54x71.png" alt="" width="54" height="71"></li>
                <li data-large-image="images/shop-02-420x550.png"><img src="images/shop-02-10x71.png" alt="" width="10" height="71"></li>
              </ul> --}}
            </div>
            <div class="unit-right product-single-image">
              <div class="product-single-image-element">
              <canvas class="product-image-area animateImageIn" id="the-canvas-{{$item->id}}" style=" direction: ltr;"></canvas>
            </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-7 col-xl-6 col-xxl-6 text-center text-lg-left">
      
        
        @foreach($item->penulis as $key => $penulis)
        <div class="heading-5 d-inline">{{$penulis->nama}}
       
          @if($key +1< count($item->penulis)) 
          ,
        @endif

        </div>

 
          
        @endforeach
        <h3>{{$item->judul}}</h3>
        <div class="divider divider-default"></div>
        <p class="text-spacing-sm">{{$item->summary}}</p>
        {{-- <ul class="inline-list">
          <li class="text-center"><span class="icon novi-icon icon-md mdi mdi-star icon-gray-light"></span>
            <p class="text-spacing-sm offset-0">Bestseller<br>2016</p>
          </li>
          <li class="text-center"><span class="icon novi-icon icon-md mdi mdi-trophy icon-gray-light"></span>
            <p class="text-spacing-sm offset-0">Bestseller<br>2016</p>
          </li>
        </ul> --}}
        <ul class="inline-list">
          {{-- <li class="text-middle">
            <h6>$29.00</h6>
          </li> --}}
          <li class="text-middle"><a class="button button-sm button-primary button-nina" href="{{$item->link}}">Read .pdf</a></li>
          {{-- <li class="text-middle"><a target="_blank" type="application/octet-stream" class="button button-sm button-default-outline button-nina"  download="download.pdf"  href="{{$item->link}}.pdf">Download</a></li> --}}
        </ul>
      </div>
    </div>
  </div>
</section>


<section class="section section-lg bg-gray-100 text-center">
  <div class="container container-wide">
    <h3>Similar Research</h3>
    <div class="divider divider-default"></div>
    
      <form action="/respon" method="POST" class="row row-30">
        @csrf
        <input type="hidden" name="id" value="{{$item->id}}">
@foreach( $doc_sim as $data)
      <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3">
        <div class="product">
          <input type="checkbox" name="respon-{{$data->id}}">
          <input hidden type="hidden" name="item[]" value="{{$data->id}}">

          <div class="product-image"><a href="/{{$data->uid}}">
            <canvas id="the-canvas-{{$data->id}}" style=" direction: ltr;" width="188" height="246"></canvas>
          </a>
          </div>
          <div class="product-title">
            <h5><a href="/{{$data->uid}}">{{$data->judul}}<br class="d-none d-xxl-block">&nbsp;
               </a></h5>
          </div>
    
          <div class="product-button"><a class="button button-primary button-nina" href="/{{$data->uid}}">Read</a></div>
        </div>
      </div>

      @endforeach

      <div class="text-center container"><button class="button button-primary" type="submit">Submit Response</button></div>
      
    </form>
  </div>
</section>



      <!-- Small Features-->
      
  
      @endsection
      <!-- Page Footer-->
      <!-- Footer Default-->
     

      @push('scripts')

        {{-- <script src="{{asset('/js/pdf_thumbnail.js')}}"></script> --}}
        {{-- <script src="{{asset('/js/pdf.js')}}"></script> --}}


        <script src="{{asset('js/pdf.js')}}"></script>





        <script id="script">
          //
          // If absolute URL from the remote server is provided, configure the CORS
          // header on that server.
          //
        pdfjsLib.GlobalWorkerOptions.workerSrc = "{{asset('/js/pdf.worker.js')}}";
        
        
          //
          // The workerSrc property shall be specified.
          //
        
          //
          // Asynchronous download PDF
          //
          
          var proxyurl = "https://cors-anywhere.herokuapp.com/";
          var url = '{{$item->link}}.pdf';
        
          var loadingTask = pdfjsLib.getDocument(proxyurl+url);
        
          loadingTask.promise.then(function(pdf) {
            //
            // Fetch the first page
            //
            pdf.getPage(1).then(function(page) {
              var scale = 1;
              var viewport = page.getViewport({ scale: scale, });
        
              //
              // Prepare canvas using PDF page dimensions
              //
              var canvas = document.getElementById('the-canvas-{{$item->id}}');
              var context = canvas.getContext('2d');
              canvas.height = viewport.height;
              canvas.width = viewport.width;
        
              //
              // Render PDF page into canvas context
              //
              var renderContext = {
                canvasContext: context,
                viewport: viewport,
              };
              page.render(renderContext);
            });
          });
        
        

        
  @foreach ($doc_sim as $karya)
  var proxyurl = "https://cors-anywhere.herokuapp.com/";
  var url = '{{App\KaryaIlmiah::find($karya->id)->link}}.pdf';

console.log(url)

  var loadingTask = pdfjsLib.getDocument(proxyurl+url);

  loadingTask.promise.then(function(pdf) {
    //
    // Fetch the first page
    //
    pdf.getPage(1).then(function(page) {
      var scale = 0.3;
      var viewport = page.getViewport({ scale: scale, });

      //
      // Prepare canvas using PDF page dimensions
      //
      var canvas = document.getElementById('the-canvas-{{$karya->id}}');
      var context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      //
      // Render PDF page into canvas context
      //
      var renderContext = {
        canvasContext: context,
        viewport: viewport,
      };
      page.render(renderContext);
    });
  });


  @endforeach

        </script>
        

     
        
      @endpush
  
 