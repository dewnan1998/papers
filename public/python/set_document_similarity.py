from database import getKaryaIlmiah 
from preprocessing import tokenize
from  karya_ilmiah import KaryaIlmiah
import numpy as np
from gensim.models import Word2Vec
from pathlib import Path
import dill

def get_mean_vector(word2vec_model, words):
    # remove out-of-vocabulary words
    words = [word for word in words if word in word2vec_model.wv.vocab]
    if len(words) >= 1:
        return np.mean(word2vec_model[words], axis=0)
    else:
        return []

def _cosine_sim(vecA, vecB):
    """Find the cosine similarity distance between two vectors."""
    csim = np.dot(vecA, vecB) / (np.linalg.norm(vecA) * np.linalg.norm(vecB))
    if np.isnan(np.sum(csim)):
        return 0
    return csim

def calculate_similarity(karya_ilmiah, target_karya_ilmiah, threshold=0):      
    results =[]
    source_vec = karya_ilmiah.vector
    for item in target_karya_ilmiah:

        if karya_ilmiah.id == item.id  :
          target_vec = item.vector

        sim_score = _cosine_sim(source_vec, target_vec)
        if sim_score > threshold:
            results.append({"score": sim_score, "doc": item.summary ,"id" : item.id})
        # Sort results by score in desc order
        results.sort(key=lambda k: k["score"], reverse=True)

    return results[0:5]



m4 = Word2Vec.load('word2vec-4.model')





try:
    with open('karya_ilmiah.pkl', 'rb') as f:
        print ("Load karya ilmiah . . .")
        karya_ilmiah = dill.load(f)

except (OSError, IOError) as e:

    data = getKaryaIlmiah()

    karya_ilmiah = []
    for item in data :

        karya = KaryaIlmiah(item)
    
    # print(get_mean_vector(m4,item['summary']))
        # mean = get_mean_vector(m4,tokenize(item['summary']))
        # karya.setVector(mean)
        karya_ilmiah.append(karya)
    


    print ("Karya ilmiah diset")

    with open('karya_ilmiah.pkl', 'wb') as f:
        dill.dump(karya_ilmiah, f)





print ("get rekomendasi")


data_folder = Path("document_similar_1000/")

filename = 'karya_ilmiah-'+str(karya_ilmiah[0].id)+'.pkl'


file_to_open = data_folder / filename

print(file_to_open)

import time
start_time = time.time()
temp_time = time.time()

from DocSim import DocSim
ds = DocSim(m4)


for item in karya_ilmiah[0:1000] :

    try:
        data_folder = Path("document_similar_1000/")

        filename = 'karya_ilmiah-'+str(item.id)+'.pkl'

        file_to_open = data_folder /  filename

        print(file_to_open)

        with open(file_to_open, 'rb') as f:
            print ("Load Rekomendasi . . .")
            sim_result = dill.load(f)

    except (OSError, IOError) as e:
        print("Membuat rekomendasi dokumen "+str(item.id))

      
    

        sim_result = ds.calculate_similarity(item, karya_ilmiah[0:1000])

        print(sim_result[0:5])

        data_folder = Path("document_similar_1000/")

        filename = 'karya_ilmiah-'+str(item.id)+'.pkl'

        file_to_open = data_folder /  filename

      



        with open(file_to_open, 'wb') as f:
            dill.dump(sim_result, f)

        print("Dokumen berhasil dibuat")
        print("--- %s seconds ---" % (time.time() - temp_time))
        temp_time = time.time()
