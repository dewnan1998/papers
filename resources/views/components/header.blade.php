
<header class="section page-header">
    <!-- RD Navbar-->
    <div class="rd-navbar-wrap rd-navbar-shop-header">
      <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fullwidth" data-xl-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-md-stick-up-offset="100px" data-lg-stick-up-offset="150px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true" data-xl-stick-up="true">
        <div class="rd-navbar-top-panel novi-background">
          <div class="rd-navbar-nav-wrap">
            <!-- RD Navbar Nav-->
            <ul class="rd-navbar-nav">
              <li class="active"><a href="/">Home</a>
              </li>
              <li><a href="#">About</a>
                <!-- RD Navbar Dropdown-->
              </li>
        
       
              <li><a href="contacts.html">Contacts</a>

              {{-- <li><a href="contacts.html">Donate</a> --}}

              </li>
            </ul>
          </div>
        </div>
        <div class="rd-navbar-inner">
          <!-- RD Navbar Panel-->
          <div class="rd-navbar-panel">
            <!-- RD Navbar Toggle-->
            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
            <!-- RD Navbar Brand-->
            <div class="rd-navbar-brand"><a class="brand-name" href="/"><img class="logo-default" src={{asset("images/logo5.png")}} alt="" width="159" height="63"/><img class="logo-inverse" src="images/logo-inverse-127x38.png" alt="" width="127" height="38"/></a></div>
          </div>
          <div class="rd-navbar-aside-center">
            <!-- RD Navbar Search-->
            <div class="rd-navbar-search"><a class="rd-navbar-search-toggle" data-rd-navbar-toggle=".rd-navbar-search" href="#"><span></span></a>
              <form class="rd-search" action="/search"  method="GET">
                <div class="rd-mailform-inline rd-mailform-sm rd-mailform-inline-modern">
                  <div class="rd-mailform-inline-inner">
                    <div class="form-wrap form-wrap-icon mdi-magnify">
                      <label class="form-label form-label" for="rd-navbar-search-form-input">Search...</label>
                      <input class="rd-navbar-search-form-input form-input" id="rd-navbar-search-form-input" type="text" name="s" value="{{Request::get('s')}}" autocomplete="off">
                      <div id="rd-search-results-live"></div>
                    </div>
                    <button class="rd-search-form-submit rd-search-form-submit-icon mdi mdi-magnify"></button>
                    <button class="rd-search-form-submit button form-button button-sm button-primary button-nina">search</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="rd-navbar-aside-right">
            {{-- <div class="rd-navbar-shop rd-navbar-login"><a class="rd-navbar-shop-icon mdi mdi-login" href="login-page.html"><span class="d-none d-xl-inline">Login</span></a></div> --}}
          </div>
        </div>
      </nav>
    </div>
  </header>