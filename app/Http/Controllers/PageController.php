<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KaryaIlmiah;
use App\Pembahasan;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\InteraksiUserKaryaIlmiah;
class PageController extends Controller
{
    //

    public function index()
    {

        return view('index');
    }

    public function item($uid)
    {
        $item = KaryaIlmiah::where('uid', $uid)->first();
        // dd($item->penulis);

        

        $x = exec(' cd C:\Users\dewna\Documents\Skripsi\project\public\python && python C:\Users\dewna\Documents\Skripsi\project\public\python\get_document_similarity.py '. str_replace( ' ','+',$item->judul));
        // dd($x);

        $doc_sim = json_decode($x);

        // dd($x);


        return view('item.index')->with('item', $item)->with('doc_sim' ,$doc_sim);
    }


    public function search(Request $request)
    {
        // dd($item->penulis);

    //    dd(  str_replace( ' ','+',$request->s));

        $x = exec(' cd C:\Users\dewna\Documents\Skripsi\project\public\python && python C:\Users\dewna\Documents\Skripsi\project\public\python\search_document.py '.  str_replace( ' ','+',$request->s));
        // dd($x);

        $doc_sim = json_decode($x);


        return view('search_result')->with('document' ,$doc_sim);
    }


    public function sendResponse(Request $request)
    {
        $i = 0;
        foreach ($request->item as $data)
        {

           $value = isset($_POST['respon-'.$data]) ? 1 : 0;
             


         InteraksiUserKaryaIlmiah::updateOrCreate([
            'karya_ilmiah_id' => $request->id,
            'respon_karya_ilmiah_id' => $data,
            'user_id' => Auth::user()->id,

         ],
         [
                
                'value' =>$value,
            ]);
            $i++;
        }

        return redirect()->back();
    }

    public function responseList()
    {
        return view('response_list');
    }
}
