<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <!-- Site Title-->
    <title>Home</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <script src={{asset("/cdn-cgi/apps/head/3ts2ksMwXvKRuG480KNifJ2_JNM.js")}}></script><link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald:200,400%7CLato:300,400,300italic,700%7CMontserrat:900">
    <link rel="stylesheet" href={{asset("css/bootstrap.css")}}>
    <link rel="stylesheet" href={{asset("css/style.css")}}>
    <link rel="stylesheet" href={{asset("css/fonts.css")}}>
  </head>
  <body>
    <!-- Page preloader-->
    <div class="page-loader">
      <div class="page-loader-body">
        <div class="cube-loader">
          <div class="cube loader-1"></div>
          <div class="cube loader-2"></div>
          <div class="cube loader-4"></div>
          <div class="cube loader-3"></div>
        </div>
      </div>
    </div>
    <!-- Page-->
    <div class="page">
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap rd-navbar-shop-header">
          <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fullwidth" data-xl-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-md-stick-up-offset="100px" data-lg-stick-up-offset="150px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true" data-xl-stick-up="true">
            <div class="rd-navbar-top-panel novi-background">
              <div class="rd-navbar-nav-wrap">
                <!-- RD Navbar Nav-->
                <ul class="rd-navbar-nav">
                  <li class="active"><a href="index.html">Home</a>
                  </li>
                  <li><a href="#">About Us</a>
                    <!-- RD Navbar Dropdown-->
                    <ul class="rd-navbar-dropdown">
                      <li><a href="about-us.html">About Us</a>
                      </li>
                      <li><a href="about-me.html">About Me</a>
                      </li>
                    </ul>
                  </li>
                  <li><a href="#">Catalog</a>
                    <!-- RD Navbar Dropdown-->
                    <ul class="rd-navbar-dropdown">
                      <li><a href="shop-3-columns-layout.html">Shop 3 Columns Layout</a>
                      </li>
                      <li><a href="shop-4-columns-layout.html">Shop 4 Columns Layout</a>
                      </li>
                      <li><a href="checkout.html">Checkout</a>
                      </li>
                      <li><a href="product-page.html">Product Page</a>
                      </li>
                      <li><a href="shopping-cart.html">Shopping Cart</a>
                      </li>
                    </ul>
                  </li>
                  <li><a href="#">Services</a>
                    <!-- RD Navbar Dropdown-->
                    <ul class="rd-navbar-dropdown">
                      <li><a href="services.html">Services</a>
                      </li>
                      <li><a href="services-variant-2.html">Services variant 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a href="#">Blog</a>
                    <!-- RD Navbar Dropdown-->
                    <ul class="rd-navbar-dropdown">
                      <li><a href="classic-blog.html">Classic Blog</a>
                      </li>
                      <li><a href="grid-blog.html">Grid Blog</a>
                      </li>
                      <li><a href="masonry-blog.html">Masonry Blog</a>
                      </li>
                      <li><a href="modern-blog.html">Modern Blog</a>
                      </li>
                      <li><a href="audio-post.html">Audio Post</a>
                      </li>
                      <li><a href="image-post.html">Image Post</a>
                      </li>
                      <li><a href="single-post.html">Single Post</a>
                      </li>
                      <li><a href="video-post.html">Video Post</a>
                      </li>
                    </ul>
                  </li>
                  <li><a href="#">Pages</a>
                    <!-- RD Navbar Megamenu-->
                    <ul class="rd-navbar-megamenu rd-navbar-megamenu-banner">
                      <li><img src="images/megamenu-banner-301x510.jpg" alt="" width="301" height="510"/>
                      </li>
                      <li>
                        <ul class="rd-megamenu-list">
                          <li><a href="404-page.html">404 Page</a></li>
                          <li><a href="503-page.html">503 Page</a></li>
                          <li><a href="coming-soon.html">Coming Soon</a></li>
                          <li><a href="accordions.html">Accordions</a></li>
                          <li><a href="buttons.html">Buttons</a></li>
                          <li><a href="our-history.html">Our History</a></li>
                          <li><a href="forms.html">Forms</a></li>
                        </ul>
                      </li>
                      <li>
                        <ul class="rd-megamenu-list">
                          <li><a href="login-page.html">Login Page</a></li>
                          <li><a href="registration-page.html">Registration Page</a></li>
                          <li><a href="search-results.html">Search Results</a></li>
                          <li><a href="under-construction.html">Under Construction</a></li>
                          <li><a href="tables.html">Tables</a></li>
                          <li><a href="tabs.html">Tabs</a></li>
                          <li><a href="typography.html">Typography</a></li>
                        </ul>
                      </li>
                      <li>
                        <ul class="rd-megamenu-list">
                          <li><a href="fullwidth-gallery-hover-title.html">Fullwidth Gallery Hover Title</a></li>
                          <li><a href="fullwidth-gallery-inside-title.html">Fullwidth Gallery Inside Title</a></li>
                          <li><a href="grid-gallery-hover-title.html">Grid Gallery Hover Title</a></li>
                          <li><a href="grid-gallery-inside-title.html">Grid Gallery Inside Title</a></li>
                          <li><a href="grid-gallery-outside-title.html">Grid Gallery Outside Title</a></li>
                          <li><a href="masonry-gallery-hover-title.html">Masonry Gallery Hover Title</a></li>
                          <li><a href="masonry-gallery-inside-title.html">Masonry Gallery Inside Title</a></li>
                          <li><a href="masonry-gallery-outside-title.html">Masonry Gallery Outside Title</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <li><a href="contacts.html">Contacts</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand"><a class="brand-name" href="index.html"><img class="logo-default" src="images/logo-139x43.png" alt="" width="139" height="43"/><img class="logo-inverse" src="images/logo-inverse-127x38.png" alt="" width="127" height="38"/></a></div>
              </div>
              <div class="rd-navbar-aside-center">
                <!-- RD Navbar Search-->
                <div class="rd-navbar-search"><a class="rd-navbar-search-toggle" data-rd-navbar-toggle=".rd-navbar-search" href="#"><span></span></a>
                  <form class="rd-search" action="search-results.html" data-search-live="rd-search-results-live" method="GET">
                    <div class="rd-mailform-inline rd-mailform-sm rd-mailform-inline-modern">
                      <div class="rd-mailform-inline-inner">
                        <div class="form-wrap form-wrap-icon mdi-magnify">
                          <label class="form-label form-label" for="rd-navbar-search-form-input">Search...</label>
                          <input class="rd-navbar-search-form-input form-input" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off">
                          <div id="rd-search-results-live"></div>
                        </div>
                        <button class="rd-search-form-submit rd-search-form-submit-icon mdi mdi-magnify"></button>
                        <button class="rd-search-form-submit button form-button button-sm button-primary button-nina">search</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="rd-navbar-aside-right">
                <div class="rd-navbar-shop rd-navbar-login"><a class="rd-navbar-shop-icon mdi mdi-login" href="login-page.html"><span class="d-none d-xl-inline">Login</span></a></div>
              </div>
            </div>
          </nav>
        </div>
      </header>

      <!-- Swiper-->
      <section class="section swiper-container swiper-slider swiper-type-1" data-loop="true" data-autoplay="5500" data-simulate-touch="false" data-slide-effect="fade">
        <div class="swiper-wrapper bg-gray-darker">
          <div class="swiper-slide" data-slide-bg="images/home-slide-1-1920x750.jpg">
            <div class="swiper-slide-caption">
              <div class="section-xl">
                <div class="container container-wide">
                  <div class="row row-fix">
                    <div class="col-md-8 col-lg-7 col-xl-6 col-xxl-5 offset-xl-1">
                      <p class="big">Welcome to Booker</p>
                      <h3>Rent New and Used Textbooks</h3>
                      <p class="big">We provide huge discounts on all kinds of textbooks. On our website you can find lots of books for study and self-development.</p><a class="button button-primary button-nina" href="shop-4-columns-layout.html">View catalog</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide" data-slide-bg="images/home-slide-2-1920x750.jpg">
            <div class="swiper-slide-caption">
              <div class="section-xl">
                <div class="container container-wide">
                  <div class="row row-fix">
                    <div class="col-md-8 col-lg-7 col-xl-6 col-xxl-5 offset-xl-1">
                      <p class="big">Diverse Book Collection</p>
                      <h3>Find your Favorite books here</h3>
                      <p class="big">From applied literature to educational resources, we have a lot of textbooks to offer you. We provide only the best books for rent.</p><a class="button button-primary button-nina" href="shop-4-columns-layout.html">View catalog</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide" data-slide-bg="images/home-slide-3-1920x750.jpg">
            <div class="swiper-slide-caption">
              <div class="section-xl">
                <div class="container container-wide">
                  <div class="row row-fix">
                    <div class="col-md-8 col-lg-7 col-xl-6 col-xxl-5 offset-xl-1">
                      <p class="big">Only for Our Website Visitors</p>
                      <h3>Exclusive Book Offers</h3>
                      <p class="big">We regularly update our catalog with exclusive book offers from international authors and publishing houses.</p><a class="button button-primary button-nina" href="shop-4-columns-layout.html">View catalog</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Swiper controls-->
        <div class="swiper-buttons">
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>
        </div>
      </section>

      <!-- Button isotope-->
      <section class="section section-variant-2 bg-default novi-background bg-cover text-center">
        <div class="container container-wide">
          <div class="isotope-wrap row row-fix row-50">
            <!-- Isotope Filters-->
            <div class="col-xl-12">
              <ul class="isotope-filters isotope-filters-horizontal">
                <li class="block-top-level">
                  <p class="big">Choose your category:</p>
                  <!-- Isotope Filters-->
                  <button class="isotope-filters-toggle button button-xs button-primary" data-custom-toggle="#isotope-filters-list-1" data-custom-toggle-hide-on-blur="true">Filter<span class="caret"></span></button>
                  <ul class="isotope-filters-list isotope-filters-list-buttons" id="isotope-filters-list-1">
                    <li><a class="button-xs active" data-isotope-filter="*" data-isotope-group="movies" href="#">All Categories</a></li>
                    <li><a class="button-xs" data-isotope-filter="type 1" data-isotope-group="movies" href="#">Business & Economics</a></li>
                    <li><a class="button-xs" data-isotope-filter="type 2" data-isotope-group="movies" href="#">Web Design</a></li>
                    <li><a class="button-xs" data-isotope-filter="type 3" data-isotope-group="movies" href="#">Marketing</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <!-- Isotope Content-->
            <div class="col-xl-12">
              <div class="isotope isotope-md row row-30" data-isotope-layout="fitRows" data-isotope-group="movies" data-lightgallery="group">
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 3">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-01-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">Immutable Laws<br class="d-none d-xxl-block">&nbsp;of Marketing</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$27.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 2">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-02-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">The Art of<br class="d-none d-xxl-block">&nbsp;Leadership</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$25.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 1">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-03-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">The Basics of<br class="d-none d-xxl-block">&nbsp;Web Design</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$21.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 3">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-04-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">Grid Systems in<br class="d-none d-xxl-block">&nbsp;Web Design</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$29.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 2">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-05-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">Tools That Reveal Why Your Users Abandon Your Website</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$25.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 1">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-06-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">Dangerous Side Effects<br class="d-none d-xxl-block">&nbsp;
                          of Bad Customer Service</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$21.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 3">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-07-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">Intro Guide to UX Reviews<br class="d-none d-xxl-block">&nbsp;
                          for Web Designers</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$29.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3 isotope-item" data-filter="type 2">
                  <div class="product">
                    <div class="product-image"><a href="product-page.html"><img src="images/book-08-188x246.jpg" alt="" width="188" height="246"/></a></div>
                    <div class="product-title">
                      <h5><a href="product-page.html">How to Create a Web Design<br class="d-none d-xxl-block">&nbsp;
                          Style Guide</a></h5>
                    </div>
                    <div class="product-price">
                      <h6>$29.00</h6>
                    </div>
                    <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div><a class="button button-primary button-nina" href="shop-3-columns-layout.html">view more books</a>
        </div>
      </section>

      <!-- Small Features-->
      <section class="section section-variant-2 bg-gray-100 novi-background bg-cover">
        <div class="container container-wide">
          <div class="row row-50 justify-content-sm-center text-gray-light">
            <div class="col-sm-10 col-md-6 col-xl-3">
              <article class="box-minimal">
                <div class="box-minimal-header">
                  <div class="box-minimal-icon novi-icon fl-bigmug-line-favourites5"></div>
                  <h6 class="box-minimal-title">Free Shipping</h6>
                </div>
                <p>Booker offers free shipping worldwide, which makes it possible for you to rent a book from us even if you live far away from our head office. Shipping process is fast and secure.</p>
              </article>
            </div>
            <div class="col-sm-10 col-md-6 col-xl-3">
              <article class="box-minimal">
                <div class="box-minimal-header">
                  <div class="box-minimal-icon novi-icon fl-bigmug-line-two316"></div>
                  <h6 class="box-minimal-title">Easy Returns</h6>
                </div>
                <p>You can always return a book after you’ve read it. Just use a unique link that can be found inside our every textbook. Then fill out a special form and send the book to one of our offices.</p>
              </article>
            </div>
            <div class="col-sm-10 col-md-6 col-xl-3">
              <article class="box-minimal">
                <div class="box-minimal-header">
                  <div class="box-minimal-icon novi-icon fl-bigmug-line-note35"></div>
                  <h6 class="box-minimal-title">Take Notes</h6>
                </div>
                <p>Textbooks rented at Booker have a special section where you can take notes. However, we ask all our customers to limit your writing and highlighting to a minimal amount.</p>
              </article>
            </div>
            <div class="col-sm-10 col-md-6 col-xl-3">
              <article class="box-minimal">
                <div class="box-minimal-header">
                  <div class="box-minimal-icon novi-icon fl-bigmug-line-circular220"></div>
                  <h6 class="box-minimal-title">satisfaction guaranteed</h6>
                </div>
                <p>We hope that you will like our book rental service. Our team makes every effort to offer you an easy and enjoyable experience of renting a textbook at any time of the year.</p>
              </article>
            </div>
          </div>
        </div>
      </section>

      <!-- Twitter form-->
      <section class="section section-variant-2 bg-image-11 novi-background custom-bg-image context-dark">
        <div class="container container-bigger">
          <div class="row row-fix row-50 justify-content-lg-between">
            <div class="col-sm-12 col-md-6 col-lg-5">
              <h3>get the latest news from the team of Booker</h3>
              <hr class="divider divider-left divider-default">
              <p class="big">Keep up with our always upcoming news, updates, and publications. Enter your e-mail and subscribe to our newsletter.</p>
              <!-- Subscribe form-->
              <form class="rd-mailform" data-form-output="form-output-global" action="bat/rd-mailform.php" method="post" data-form-type="subscribe">
                <div class="form-wrap">
                  <input class="form-input" type="email" name="email" data-constraints="@Email @Required" id="form-email-twitter">
                  <label class="form-label" for="form-email-twitter">Enter your e-mail</label>
                </div>
                <button class="button form-button button-primary button-nina" type="submit">Subscribe</button>
              </form>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-5">
              <h3>follow us for news</h3>
              <hr class="divider divider-left divider-default">
              <!-- RD Twitter Feed-->
              <div class="twitter" data-twitter-username="templatemonster" data-twitter-date-hours=" hours ago" data-twitter-date-minutes=" minutes ago">
                <div class="twitter-sm" data-twitter-type="tweet">
                  <div class="twitter-box-icon">
                    <div class="twitter-date text-dark small"></div>
                  </div>
                  <div class="twitter-box-text">
                    <div class="twitter-text" data-tweet="text"></div><span class="text-middle" data-date="text"></span>
                  </div>
                </div>
                <div class="twitter-sm" data-twitter-type="tweet">
                  <div class="twitter-box-icon">
                    <div class="twitter-date text-dark small"></div>
                  </div>
                  <div class="twitter-box-text">
                    <div class="twitter-text" data-tweet="text"></div><span class="text-middle" data-date="text"></span>
                  </div>
                </div>
                <div class="twitter-sm" data-twitter-type="tweet">
                  <div class="twitter-box-icon">
                    <div class="twitter-date text-dark small"></div>
                  </div>
                  <div class="twitter-box-text">
                    <div class="twitter-text" data-tweet="text"></div><span class="text-middle" data-date="text"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- news and reviews-->
      <section class="section section-variant-2 bg-gray-100 novi-background bg-cover text-center">
        <div class="container container-bigger">
          <h3>news and reviews</h3>
          <div class="divider divider-default"></div>
          <div class="row row-50 justify-content-sm-center justify-content-lg-around justify-content-xxl-between offset-custom-2">
            <div class="col-md-6 col-xl-6 col-xxl-7">
              <article class="post-blog-large">
                <figure class="post-blog-large-image"><img src="images/landing-movie-13-868x640.jpg" alt="" width="868" height="640"/>
                </figure>
                <ul class="post-blog-meta">
                  <li><span>by</span>&nbsp;<a href="about-me.html">Ronald Chen</a></li>
                  <li>
                    <time datetime="2018">Feb, 27 2018 at 5:47 pm</time>
                  </li>
                </ul>
                <div class="post-blog-large-caption">
                  <ul class="post-blog-tags">
                    <li><a class="button-tags" href="single-post.html">News</a></li>
                  </ul><a class="post-blog-large-title" href="single-post.html">Top 5 Web Design and Development Books to Read This Year</a>
                  <p class="post-blog-large-text">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure</p><a class="button button-xs button-primary" href="single-post.html">Continue reading</a>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-xl-4">
              <div class="post-minimal-wrap">
                <!-- Post minimal-->
                <article class="post-minimal post-minimal-md">
                  <p class="post-minimal-title"><a href="single-post.html">10 Reasons to Rent a Textbook</a></p>
                  <time class="post-minimal-time" datetime="2018">Feb 27, 2018 at 5:47 pm</time>
                </article>
                <!-- Post minimal-->
                <article class="post-minimal post-minimal-md">
                  <p class="post-minimal-title"><a href="single-post.html">Why Textbooks are More Beneficial than eBooks</a></p>
                  <time class="post-minimal-time" datetime="2018">Feb 27, 2018 at 5:47 pm</time>
                </article>
                <!-- Post minimal-->
                <article class="post-minimal post-minimal-md">
                  <p class="post-minimal-title"><a href="single-post.html">Should I Buy or Rent Textbooks?</a></p>
                  <time class="post-minimal-time" datetime="2018">Feb 27, 2018 at 5:47 pm</time>
                </article>
                <!-- Post minimal-->
                <article class="post-minimal post-minimal-md">
                  <p class="post-minimal-title"><a href="single-post.html">Textbooks Used to Promote Global Peace</a></p>
                  <time class="post-minimal-time" datetime="2018">Feb 27, 2018 at 5:47 pm</time>
                </article>
                <!-- Post minimal-->
                <article class="post-minimal post-minimal-md">
                  <p class="post-minimal-title"><a href="single-post.html">What to Read This Summer</a></p>
                  <time class="post-minimal-time" datetime="2018">Feb 27, 2018 at 5:47 pm</time>
                </article>
              </div>
            </div>
          </div><a class="button button-primary button-nina" href="classic-blog.html">view all News</a>
        </div>
      </section>

      <!-- Icon carousel-->
      <section class="section bg-default novi-background novi-section">
        <div class="container-custom">
          <div class="row justify-content-md-center justify-content-xl-start row-0">
            <div class="col-md-10 col-lg-6">
              <div class="section-variant-2">
                <div class="container-custom-inner">
                  <h3>What people say</h3>
                  <hr class="divider divider-default">
                  <!-- Slick Carousel-->
                  <div class="slider-widget">
                    <!-- Owl Carousel-->
                    <div class="owl-carousel-widget owl-carousel" data-custom-nav="true" data-items="1" data-dots="false" data-nav="false" data-stage-padding="15" data-loop="true" data-margin="0" data-mouse-drag="false">
                      <div class="item">
                        <article class="quote-classic">
                          <div class="quote-classic-header"><img class="quote-classic-image" src="images/quote-user-1-210x210.jpg" alt="" width="210" height="210"/>
                            <div class="quote-classic-meta">
                              <p class="quote-classic-cite">James Smith</p>
                              <p class="quote-classic-small">Regular Client</p>
                            </div>
                          </div>
                          <div class="quote-classic-text">
                            <p>The books I rent here always arrive on time, which is of primary importance to me as I like to read a lot. I am especially interested in books about business and science and you have a diverse collection of them. Thank you for your service!</p>
                          </div>
                        </article>
                      </div>
                      <div class="item">
                        <article class="quote-classic">
                          <div class="quote-classic-header"><img class="quote-classic-image" src="images/quote-user-2-210x210.jpg" alt="" width="210" height="210"/>
                            <div class="quote-classic-meta">
                              <p class="quote-classic-cite">Mary McMillan</p>
                              <p class="quote-classic-small">Regular Client</p>
                            </div>
                          </div>
                          <div class="quote-classic-text">
                            <p>As an avid reader and web developer, I always use your website to find and rent new books. Sometimes I even rent exclusives that are not available at bookstores. I especially like your collection of JavaScript and PHP guides, they are very helpful.</p>
                          </div>
                        </article>
                      </div>
                      <div class="item">
                        <article class="quote-classic">
                          <div class="quote-classic-header"><img class="quote-classic-image" src="images/quote-user-3-210x210.jpg" alt="" width="210" height="210"/>
                            <div class="quote-classic-meta">
                              <p class="quote-classic-cite">Samuel Contreras</p>
                              <p class="quote-classic-small">Regular Client</p>
                            </div>
                          </div>
                          <div class="quote-classic-text">
                            <p>Thanks to Booker, I saved over $140 just on one book! The customer service was top-notch and everyone there was very friendly. I will be back for next semester and I will definitely tell my friends about your website. Thanks a lot for your great customer service!</p>
                          </div>
                        </article>
                      </div>
                    </div>
                    <div class="slider-widget-aside">
                      <div class="icon icon-lg-smaller mdi mdi-format-quote"></div>
                      <div class="owl-carousel-widget-nav">
                        <button class="slick-prev mdi mdi-arrow-left-bold-circle" type="button" data-slick="slick-1"></button>
                        <button class="slick-next mdi mdi-arrow-right-bold-circle" type="button" data-slick="slick-1"></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 bg-image custom-bg-image" style="background-image: url(images/landing-maritime-8-1132x832.jpg);">
              <div class="section-lg">
                <div class="container-custom-inner container-custom-inner-1 text-center">
                  <div class="row row-50 row-xl-120">
                    <div class="col-sm-6 col-md-3 col-lg-6">
                      <div class="counter-wrap"><span class="icon novi-icon icon-secondary mdi mdi-television-guide"></span>
                        <div class="heading-3"><span class="counter" data-speed="2500">62000</span><span class="counter-preffix">+</span></div>
                        <p>Textbooks</p>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-6">
                      <div class="counter-wrap"><span class="icon novi-icon icon-secondary mdi mdi-account-multiple-outline"></span>
                        <div class="heading-3"><span class="counter" data-step="2500">2466</span></div>
                        <p>Active Readers</p>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-6">
                      <div class="counter-wrap"><span class="icon novi-icon icon-secondary mdi mdi-clock"></span>
                        <div class="heading-3"><span class="counter" data-speed="2500">12</span><span class="counter-preffix">+</span></div>
                        <p>Awards</p>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-6">
                      <div class="counter-wrap"><span class="icon novi-icon icon-secondary mdi mdi-code-tags"></span>
                        <div class="heading-3"><span class="counter" data-step="2500">520</span></div>
                        <p>Authors</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- CTA-->
      <section class="section section-xs text-center bg-gray-700 novi-background bg-cover">
        <div class="container container-wide">
          <div class="box-cta-thin">
            <p class="big"><span class="label-cta label-cta-primary">Hot!</span><span>Rent New and Used Textbooks and.&nbsp;</span><strong>Save Huge on Our Books.</strong>&nbsp;<a class="link-bold" href="#">Rent now!</a></p>
          </div>
        </div>
      </section>

      <!-- Page Footer-->
      <!-- Footer Default-->
      <footer class="section page-footer page-footer-default novi-background bg-cover text-left bg-gray-darker">
        <div class="container container-wide">
          <div class="row row-50 justify-content-sm-center">
            <div class="col-md-6 col-xl-3">
              <div class="inset-xxl">
                <h6>About Us</h6>
                <p class="text-spacing-sm">Booker is the worldwide textbook rental service created by readers for readers. We operate since 2014 and we offer lots of new and used textbooks on various topics including web design, development, business, economics, marketing, and more.</p>
              </div>
            </div>
            <div class="col-md-6 col-xl-2">
              <h6>Quick links</h6>
              <ul class="list-marked list-marked-primary">
                <li><a href="about-us.html">About</a></li>
                <li><a href="shop-3-columns-layout.html">Catalog</a></li>
                <li><a href="shop-4-columns-layout.html">How To Rent</a></li>
                <li><a href="classic-blog.html">Blog</a></li>
                <li><a href="#">Pages</a></li>
                <li><a href="contacts.html">Contacts</a></li>
              </ul>
            </div>
            <div class="col-md-6 col-xl-4">
              <h6>Gallery</h6>
              <div class="instafeed text-center" data-lightgallery="group">
                <div class="row row-10 row-narrow">
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-01-1200x800-original.jpg"><img src="images/footer-gallery-01-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-02-1200x800-original.jpg"><img src="images/footer-gallery-02-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-03-1200x800-original.jpg"><img src="images/footer-gallery-03-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-04-1200x800-original.jpg"><img src="images/footer-gallery-04-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-05-1200x800-original.jpg"><img src="images/footer-gallery-05-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-06-1200x800-original.jpg"><img src="images/footer-gallery-06-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-07-800x1200-original.jpg"><img src="images/footer-gallery-07-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-08-1200x800-original.jpg"><img src="images/footer-gallery-08-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-xl-3">
              <h6>newsletter</h6>
              <p class="text-spacing-sm">Keep up with our always upcoming news, events, and updates. Enter your e-mail and subscribe to our newsletter.</p>
              <!-- RD Mailform: Subscribe-->
              <form class="rd-mailform rd-mailform-inline rd-mailform-sm" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                <div class="rd-mailform-inline-inner">
                  <div class="form-wrap">
                    <input class="form-input" type="email" name="email" data-constraints="@Email @Required" id="subscribe-form-email-1"/>
                    <label class="form-label" for="subscribe-form-email-1">Enter your e-mail</label>
                  </div>
                  <button class="button form-button button-sm button-primary button-nina" type="submit">Subscribe</button>
                </div>
              </form>
            </div>
          </div>
          <p class="right">&#169;&nbsp;<span class="copyright-year"></span> All Rights Reserved&nbsp;<a href="#">Terms of Use</a>&nbsp;and&nbsp;<a href="privacy-policy.html">Privacy Policy</a></p>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src={{asset("js/core.min.js")}}></script>
    <script src={{asset("js/script.js")}}></script>
	
	<!--LIVEDEMO_00 -->

	<script type="text/javascript">
	 var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-7078796-5']);
	  _gaq.push(['_trackPageview']);
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();</script>
	
  </body><!-- Google Tag Manager --><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>