
    <!-- Page-->
    <div class="page">
      <!-- Page Header-->
      <header class="section page-header header-modern">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap rd-navbar-corporate">
          <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-md-stick-up-offset="130px" data-lg-stick-up-offset="100px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true" data-xl-stick-up="true">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand"><a class="brand-name" href="index.html"><img class="logo-default" src="images/logo-139x43.png" alt="" width="139" height="43"/><img class="logo-inverse" src="images/logo-inverse-127x38.png" alt="" width="127" height="38"/></a></div>
              </div>
              <div class="rd-navbar-aside-center">
                <div class="rd-navbar-nav-wrap">
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <li><a href="index.html">Home</a>
                    </li>
                    <li><a href="#">About Us</a>
                      <!-- RD Navbar Dropdown-->
                      <ul class="rd-navbar-dropdown">
                        <li><a href="about-us.html">About Us</a>
                        </li>
                        <li><a href="about-me.html">About Me</a>
                        </li>
                      </ul>
                    </li>
                    <li class="active"><a href="#">Catalog</a>
                      <!-- RD Navbar Dropdown-->
                      <ul class="rd-navbar-dropdown">
                        <li><a href="shop-3-columns-layout.html">Shop 3 Columns Layout</a>
                        </li>
                        <li><a href="shop-4-columns-layout.html">Shop 4 Columns Layout</a>
                        </li>
                        <li><a href="checkout.html">Checkout</a>
                        </li>
                        <li><a href="product-page.html">Product Page</a>
                        </li>
                        <li><a href="shopping-cart.html">Shopping Cart</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#">Services</a>
                      <!-- RD Navbar Dropdown-->
                      <ul class="rd-navbar-dropdown">
                        <li><a href="services.html">Services</a>
                        </li>
                        <li><a href="services-variant-2.html">Services variant 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#">Blog</a>
                      <!-- RD Navbar Dropdown-->
                      <ul class="rd-navbar-dropdown">
                        <li><a href="classic-blog.html">Classic Blog</a>
                        </li>
                        <li><a href="grid-blog.html">Grid Blog</a>
                        </li>
                        <li><a href="masonry-blog.html">Masonry Blog</a>
                        </li>
                        <li><a href="modern-blog.html">Modern Blog</a>
                        </li>
                        <li><a href="audio-post.html">Audio Post</a>
                        </li>
                        <li><a href="image-post.html">Image Post</a>
                        </li>
                        <li><a href="single-post.html">Single Post</a>
                        </li>
                        <li><a href="video-post.html">Video Post</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#">Pages</a>
                      <!-- RD Navbar Megamenu-->
                      <ul class="rd-navbar-megamenu rd-navbar-megamenu-banner">
                        <li><img src="images/megamenu-banner-301x510.jpg" alt="" width="301" height="510"/>
                        </li>
                        <li>
                          <ul class="rd-megamenu-list">
                            <li><a href="404-page.html">404 Page</a></li>
                            <li><a href="503-page.html">503 Page</a></li>
                            <li><a href="coming-soon.html">Coming Soon</a></li>
                            <li><a href="accordions.html">Accordions</a></li>
                            <li><a href="buttons.html">Buttons</a></li>
                            <li><a href="our-history.html">Our History</a></li>
                            <li><a href="forms.html">Forms</a></li>
                          </ul>
                        </li>
                        <li>
                          <ul class="rd-megamenu-list">
                            <li><a href="login-page.html">Login Page</a></li>
                            <li><a href="registration-page.html">Registration Page</a></li>
                            <li><a href="search-results.html">Search Results</a></li>
                            <li><a href="under-construction.html">Under Construction</a></li>
                            <li><a href="tables.html">Tables</a></li>
                            <li><a href="tabs.html">Tabs</a></li>
                            <li><a href="typography.html">Typography</a></li>
                          </ul>
                        </li>
                        <li>
                          <ul class="rd-megamenu-list">
                            <li><a href="fullwidth-gallery-hover-title.html">Fullwidth Gallery Hover Title</a></li>
                            <li><a href="fullwidth-gallery-inside-title.html">Fullwidth Gallery Inside Title</a></li>
                            <li><a href="grid-gallery-hover-title.html">Grid Gallery Hover Title</a></li>
                            <li><a href="grid-gallery-inside-title.html">Grid Gallery Inside Title</a></li>
                            <li><a href="grid-gallery-outside-title.html">Grid Gallery Outside Title</a></li>
                            <li><a href="masonry-gallery-hover-title.html">Masonry Gallery Hover Title</a></li>
                            <li><a href="masonry-gallery-inside-title.html">Masonry Gallery Inside Title</a></li>
                            <li><a href="masonry-gallery-outside-title.html">Masonry Gallery Outside Title</a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li><a href="contacts.html">Contacts</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="rd-navbar-aside-right">
                {{-- <div class="rd-navbar-shop rd-navbar-login"><a class="rd-navbar-shop-icon mdi mdi-login" href="login-page.html"><span class="d-none d-xl-inline">Login</span></a></div> --}}
              </div>
            </div>
          </nav>
        </div>
      </header>

      <!-- Breadcrumbs-->
      <section class="breadcrumbs-custom custom-bg-image context-dark" style="background-image: url(images/bg-fullwidth.jpg);">
        <div class="container">
          <p class="breadcrumbs-custom-subtitle">Shop </p>
          <p class="heading-1 breadcrumbs-custom-title">Single Product</p>
          <ul class="breadcrumbs-custom-path">
            <li><a href="index.html">Home</a></li>
            <li><a href="#">Catalog</a></li>
            <li class="active">Product Page</li>
          </ul>
        </div>
      </section>


      <!-- Product Page-->
      <section class="section section-lg bg-default">
        <div class="container container-bigger product-single">
          <div class="row row-fix justify-content-sm-center justify-content-lg-between row-30 align-items-lg-center">
            <div class="col-lg-5 col-xl-6 col-xxl-5">
              <div class="product-single-preview">
                <div class="unit flex-column flex-md-row align-items-md-center unit-spacing-md-midle unit--inverse unit-sm">
                  <div class="unit-body">
                    <ul class="product-thumbnails">
                      <li class="active" data-large-image="images/shop-01-420x550.png"><img src="images/shop-01-54x71.png" alt="" width="54" height="71"></li>
                      <li data-large-image="images/shop-02-420x550.png"><img src="images/shop-02-10x71.png" alt="" width="10" height="71"></li>
                    </ul>
                  </div>
                  <div class="unit-right product-single-image">
                    <div class="product-single-image-element"><img class="product-image-area animateImageIn" src="images/shop-01-420x550.png" alt=""></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-7 col-xl-6 col-xxl-6 text-center text-lg-left">
              <div class="heading-5">Joanne Schultz</div>
              <h3>immutable laws of marketing</h3>
              <div class="divider divider-default"></div>
              <p class="text-spacing-sm">In this book, written by a renowned marketing expert, you will find a compendium of twenty-two innovative rules for understanding and succeeding in the international marketplace. From the Law of Leadership to The Law of the Category, and The Law of the Mind, these valuable insights present a clear path to successful products.</p>
              <ul class="inline-list">
                <li class="text-center"><span class="icon novi-icon icon-md mdi mdi-star icon-gray-light"></span>
                  <p class="text-spacing-sm offset-0">Bestseller<br>2016</p>
                </li>
                <li class="text-center"><span class="icon novi-icon icon-md mdi mdi-trophy icon-gray-light"></span>
                  <p class="text-spacing-sm offset-0">Bestseller<br>2016</p>
                </li>
              </ul>
              <ul class="inline-list">
                <li class="text-middle">
                  <h6>$29.00</h6>
                </li>
                <li class="text-middle"><a class="button button-sm button-primary button-nina" href="shopping-cart.html">add to cart</a></li>
                <li class="text-middle"><a class="button button-sm button-default-outline button-nina" href="#">add to wishlist</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <!-- Similar products-->
      <section class="section section-lg bg-gray-100 text-center">
        <div class="container container-wide">
          <h3>Similar products</h3>
          <div class="divider divider-default"></div>
          <div class="row row-30">
            <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3">
              <div class="product">
                <div class="product-image"><a href="product-page.html"><img src="images/book-05-188x246.jpg" alt="" width="188" height="246"/></a></div>
                <div class="product-title">
                  <h5><a href="product-page.html">Tools That Reveal Why Your Users Abandon Your Website</a></h5>
                </div>
                <div class="product-price">
                  <h6>$25.00</h6>
                </div>
                <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3">
              <div class="product">
                <div class="product-image"><a href="product-page.html"><img src="images/book-06-188x246.jpg" alt="" width="188" height="246"/></a></div>
                <div class="product-title">
                  <h5><a href="product-page.html">Dangerous Side Effects<br class="d-none d-xxl-block">&nbsp;
                      of Bad Customer Service</a></h5>
                </div>
                <div class="product-price">
                  <h6>$21.00</h6>
                </div>
                <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3">
              <div class="product">
                <div class="product-image"><a href="product-page.html"><img src="images/book-07-188x246.jpg" alt="" width="188" height="246"/></a></div>
                <div class="product-title">
                  <h5><a href="product-page.html">Intro Guide to UX Reviews<br class="d-none d-xxl-block">&nbsp;
                      for Web Designers</a></h5>
                </div>
                <div class="product-price">
                  <h6>$29.00</h6>
                </div>
                <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xl-3 col-xxl-3">
              <div class="product">
                <div class="product-image"><a href="product-page.html"><img src="images/book-08-188x246.jpg" alt="" width="188" height="246"/></a></div>
                <div class="product-title">
                  <h5><a href="product-page.html">How to Create a Web Design<br class="d-none d-xxl-block">&nbsp;
                      Style Guide</a></h5>
                </div>
                <div class="product-price">
                  <h6>$29.00</h6>
                </div>
                <div class="product-button"><a class="button button-primary button-nina" href="shopping-cart.html">add to cart</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <section class="section section-xs text-center bg-gray-700 novi-background bg-cover">
        <div class="container container-wide">
          <div class="box-cta-thin">
            <p class="big"><span>Rent New and Used Textbooks and.&nbsp;</span><strong>Save Huge on Our Books.</strong>&nbsp;<a class="link-bold" href="#">Rent now!</a></p>
          </div>
        </div>
      </section>
      <!-- Footer Default-->
      <footer class="section page-footer page-footer-default novi-background bg-cover text-left bg-gray-darker">
        <div class="container container-wide">
          <div class="row row-50 justify-content-sm-center">
            <div class="col-md-6 col-xl-3">
              <div class="inset-xxl">
                <h6>About Us</h6>
                <p class="text-spacing-sm">Booker is the worldwide textbook rental service created by readers for readers. We operate since 2014 and we offer lots of new and used textbooks on various topics including web design, development, business, economics, marketing, and more.</p>
              </div>
            </div>
            <div class="col-md-6 col-xl-2">
              <h6>Quick links</h6>
              <ul class="list-marked list-marked-primary">
                <li><a href="about-us.html">About</a></li>
                <li><a href="shop-3-columns-layout.html">Catalog</a></li>
                <li><a href="shop-4-columns-layout.html">How To Rent</a></li>
                <li><a href="classic-blog.html">Blog</a></li>
                <li><a href="#">Pages</a></li>
                <li><a href="contacts.html">Contacts</a></li>
              </ul>
            </div>
            <div class="col-md-6 col-xl-4">
              <h6>Gallery</h6>
              <div class="instafeed text-center" data-lightgallery="group">
                <div class="row row-10 row-narrow">
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-01-1200x800-original.jpg"><img src="images/footer-gallery-01-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-02-1200x800-original.jpg"><img src="images/footer-gallery-02-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-03-1200x800-original.jpg"><img src="images/footer-gallery-03-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-04-1200x800-original.jpg"><img src="images/footer-gallery-04-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-05-1200x800-original.jpg"><img src="images/footer-gallery-05-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-06-1200x800-original.jpg"><img src="images/footer-gallery-06-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-07-800x1200-original.jpg"><img src="images/footer-gallery-07-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="thumbnail-instafeed thumbnail-instafeed-minimal"> <a class="instagram-link" data-lightgallery="item" href="images/footer-gallery-08-1200x800-original.jpg"><img src="images/footer-gallery-08-110x110.jpg" alt="" width="110" height="110"/>
                        <!--img.instagram-image(src='images/_blank.png', alt='', data-images-standard_resolution-url='src')--></a>
                      <div class="caption"> 
                        <ul class="list-inline">
                          <li><span class="icon novi-icon mdi mdi-plus">  </span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-xl-3">
              <h6>newsletter</h6>
              <p class="text-spacing-sm">Keep up with our always upcoming news, events, and updates. Enter your e-mail and subscribe to our newsletter.</p>
              <!-- RD Mailform: Subscribe-->
              <form class="rd-mailform rd-mailform-inline rd-mailform-sm" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                <div class="rd-mailform-inline-inner">
                  <div class="form-wrap">
                    <input class="form-input" type="email" name="email" data-constraints="@Email @Required" id="subscribe-form-email-1"/>
                    <label class="form-label" for="subscribe-form-email-1">Enter your e-mail</label>
                  </div>
                  <button class="button form-button button-sm button-primary button-nina" type="submit">Subscribe</button>
                </div>
              </form>
            </div>
          </div>
          <p class="right">&#169;&nbsp;<span class="copyright-year"></span> All Rights Reserved&nbsp;<a href="#">Terms of Use</a>&nbsp;and&nbsp;<a href="privacy-policy.html">Privacy Policy</a></p>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body><!-- Google Tag Manager --><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>