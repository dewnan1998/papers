
  @extends('include.app')

@section('content')


<section class="section section-lg bg-default text-center">
    <div class="container">
      <div class="row justify-content-sm-center">
        <div class="col-md-12 col-xl-12">
          <h3>List Response</h3>
          <div class="table-novi table-custom-responsive">
            <table class="table-custom table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>NPM</th>
                  <th>Karya Ilmiah</th>
                  <th>Response</th>
                  <th>Sesuai</th>
                </tr>
              </thead>
              <tbody>

                @php
                  $i=0;
                  $c=0;
                @endphp
                @foreach (App\KaryaIlmiah::has('interaksi')->get() as $item)
                    

                @foreach($item->interaksi as $data)

                <tr>
                  <td>{{++$i}}</td>
                  <td>{{$data->user->nama}}</td>
                  <td>{{$data->user->npm}}</td>
                  <td>{{$data->paper->judul}}</td>
                  <td>{{$data->respon->judul}}</td>
                  <td>{{$data->value}}</td>

                  @php
                    $c+= $data->value
                  @endphp

                </tr>

                  
                @endforeach
               

                @endforeach

                <tr>

                  <th><b>TOTAL </b> </th>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>{{$c}} / {{$i}} : {{($c/$i) *100}}%</td>
                </tr>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>

  
      @endsection
      <!-- Page Footer-->
      <!-- Footer Default-->
     

      @push('scripts')

      @endpush