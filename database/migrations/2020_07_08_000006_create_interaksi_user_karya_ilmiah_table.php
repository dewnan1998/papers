<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteraksiUserKaryaIlmiahTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'interaksi_user_karya_ilmiah';

    /**
     * Run the migrations.
     * @table interaksi_user_karya_ilmiah
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('value', 45)->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('karya_ilmiah_id');
            $table->unsignedInteger('respon_karya_ilmiah_id')->nullable();

            $table->unsignedInteger('jenis_intekrasi_user_id')->nullable();

            $table->index(["user_id"], 'fk_interaksi_user_karya_ilmiah_user1_idx');

            $table->index(["jenis_intekrasi_user_id"], 'fk_interaksi_user_karya_ilmiah_jenis_intekrasi_user1_idx');

            $table->index(["karya_ilmiah_id"], 'fk_interaksi_user_karya_ilmiah_karya_ilmiah1_idx');

            $table->index(["respon_karya_ilmiah_id"], 'fk_interaksi_user_respon_karya_ilmiah_karya_ilmiah1_idx');


            $table->unique(["id"], 'id_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('user_id', 'fk_interaksi_user_karya_ilmiah_user1_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('karya_ilmiah_id', 'fk_interaksi_user_karya_ilmiah_karya_ilmiah1_idx')
                ->references('id')->on('karya_ilmiah')
                ->onDelete('no action')
                ->onUpdate('no action');

                $table->foreign('respon_karya_ilmiah_id', 'fk_interaksi_user_respon_karya_ilmiah_karya_ilmiah1_idx')
                ->references('id')->on('karya_ilmiah')
                ->onDelete('no action')
                ->onUpdate('no action');


            $table->foreign('jenis_intekrasi_user_id', 'fk_interaksi_user_karya_ilmiah_jenis_intekrasi_user1_idx')
                ->references('id')->on('jenis_intekrasi_user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
