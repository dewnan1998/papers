<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryaIlmiahHasPembahasanTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'karya_ilmiah_has_pembahasan';

    /**
     * Run the migrations.
     * @table karya_ilmiah_has_pembahasan
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('karya_ilmiah_id');
            $table->unsignedInteger('pembahasan_id');

            $table->index(["pembahasan_id"], 'fk_karya_ilmiah_has_pembahasan_pembahasan1_idx');

            $table->index(["karya_ilmiah_id"], 'fk_karya_ilmiah_has_pembahasan_karya_ilmiah1_idx');


            $table->foreign('karya_ilmiah_id', 'fk_karya_ilmiah_has_pembahasan_karya_ilmiah1_idx')
                ->references('id')->on('karya_ilmiah')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('pembahasan_id', 'fk_karya_ilmiah_has_pembahasan_pembahasan1_idx')
                ->references('id')->on('pembahasan')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
