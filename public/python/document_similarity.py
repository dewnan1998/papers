from database import getKaryaIlmiah 
from preprocessing import tokenize
from  karya_ilmiah import KaryaIlmiah
import numpy as np
from gensim.models import Word2Vec
import dill


# m4 = Word2Vec.load('word2vec-4.model')
# m4 = Word2Vec.load('word2vec-41000-stem.model')
m4 = Word2Vec.load('word2vec-title-41000-stem.model')



def get_mean_vector(word2vec_model, words):
    # remove out-of-vocabulary words
    words = [word for word in words if word in word2vec_model.wv.vocab]
    if len(words) >= 1:
        return np.mean(word2vec_model[words], axis=0)
    else:
        return []   

def _cosine_sim(vecA, vecB):
    """Find the cosine similarity distance between two vectors."""
    csim = np.dot(vecA, vecB) / (np.linalg.norm(vecA) * np.linalg.norm(vecB))
    if np.isnan(np.sum(csim)):
        return 0
    return csim

def calculate_similarity(karya_ilmiah, target_karya_ilmiah, threshold=0):
        
    results =[]
    source_vec = karya_ilmiah.vector
    for item in target_karya_ilmiah:

        if karya_ilmiah.id == item.id  :
            continue
        
        target_vec = item.vector

        sim_score = _cosine_sim(source_vec, target_vec)
        if sim_score > threshold:
            results.append({"score": sim_score, "doc": item.summary ,"id" : item.id})
        # Sort results by score in desc order
        results.sort(key=lambda k: k["score"], reverse=True)

    return results[0:5]





try:
    with open('karya_ilmiah.pkl', 'rb') as f:
        print ("Load karya ilmiah . . .")
        karya_ilmiah = dill.load(f)

except (OSError, IOError) as e:

    data = getKaryaIlmiah()

    karya_ilmiah = []
    for item in data :

        karya = KaryaIlmiah(item)
        mean = get_mean_vector(m4,tokenize(item['summary']))
        karya.setVector(mean)
        karya_ilmiah.append(karya)
    


    print ("Karya ilmiah diset")

    with open('karya_ilmiah.pkl', 'wb') as f:
        dill.dump(karya_ilmiah, f)


# from DocSim import DocSim
# ds = DocSim(m4)

# source_doc = karya_ilmiah[0].summary
# target_docs = []
# for item in karya_ilmiah[1:] :
#     target_docs.append(item.summary)



sim_scores = calculate_similarity(karya_ilmiah[0], karya_ilmiah)

print(sim_scores)














