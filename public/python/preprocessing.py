import re
import string
import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
# from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
# from spacy.lang.en.stop_words import STOP_WORDS
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer


def tokenize (dataset) :
    result = []
    for data in dataset:
        # print(data)
        res = data[0].lower() #lowercase
        # print("RES 1 : " ,res)

        res =  re.sub(r'\d+', '', res) #hilangkan nomor 

        # print("RES 2 : " ,res)

        res = res.translate(str.maketrans('', '', string.punctuation))

        # res = res.translate(str.maketrans('',''), string.punctuation) #hilangkan simbol
        # res = res.translate( string.punctuation) #hilangkan simbol
        # print("RES 3 : " ,res)


        res = res.strip()  #hilangkan white space


        # print("RES 4 : " ,res)
        # stop_words = set(STOP_WORDS.words('english')) #stopword
        sw = set(stopwords.words('english'))

        tokens = word_tokenize(res)

        res = [i for i in tokens if not i in sw] #tokenize dan stop
        stemmer= PorterStemmer()
        stem = []
        # res = [i for i in data = stemmer.stem(data)]
        for word in res:
           stem.append((stemmer.stem(word)))
        lemm = []

       
        lemmatizer=WordNetLemmatizer()
        for word in stem:
             lemm.append(lemmatizer.lemmatize(word))

        result.append(res)
 
    
    return result


