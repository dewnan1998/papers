<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/login', 'AuthController@loginView')->name('login');
Route::get('/logout', 'AuthController@logout');

Route::post('/login', 'AuthController@login');

Route::get('/response','PageController@responseList' );



Route::group(['middleware'=>'auth'] , function()
{   

    
    Route::get('/', 'PageController@index');
    Route::post('/respon', 'PageController@sendResponse');
    Route::get('/search', 'PageController@search');

    Route::get('/{uid}','PageController@item'); 
});




Route::get('/test', function () {
    // phpinfo();
    return view('test');
});












//--


Route::get('/dataset/debug', function () {

    //    dd( base_path('datasets\arvixData.json'));



    $jsonString = file_get_contents(public_path('datasets/arxivData.json'));




    $data = json_decode($jsonString, false);

    dd($data[35375]);

    dump(str_replace("'name'", '"name"', $data[523]->author));

    $arr = json_decode(str_replace("'name'", '"name"', $data[523]->author)); //formated wrond json
    // echo $data[0]->summary;'
    // dd($arr);




    $date = new DateTime();
    $pub = $date->setDate($data[0]->year, $data[0]->month, $data[0]->day);
    $arr = json_decode(str_replace("'", '"', $data[0]->link)); //format

    

    $item = null;
    foreach ($arr as $struct) {
        if ('application/pdf' == $struct->type) {
            $item = $struct;
            break;
        }
    }


    $search = ["'", "None"];
$replace = ['"', '""'];

    // dd($item->href);

    $arr = json_decode(str_replace($search, $replace, $data[0]->tag));

    // dd($data[0]->tag);

    $whatIWant = substr($arr[2]->term, strpos($arr[0]->term, ".") + 1);

    foreach ($arr as $x)
    {
        $whatIWant = substr($x->term, strpos($x->term, ".") + 1);
        dump($whatIWant);
    }
    // dump( $arr);
    // dd( $whatIWant);

});



Route::get('test/seeder' , function()
{
    

    $jsonString = file_get_contents(public_path('datasets/arxivData.json'));




    $data = json_decode($jsonString, false);


    $lim = 100;
    $i = 1;
    


    foreach ($data as $obj) {


        // dd($obj);




        $date = new DateTime();

        $pub = $date->setDate($obj->year, $obj->month, $obj->day);



        $links = json_decode(str_replace("'", '"', $obj->link)); //format


        $link = null;
        foreach ($links as $temp) {
            if ('application/pdf' == $temp->type) {
                $link = $temp;
                break;
            }
        }






        $karya =  App\KaryaIlmiah::updateOrCreate(
            [
                'uid' => $obj->id,
            ],
            [
                'judul' => $obj->title,
                'summary' => $obj->summary,
                'publish' => $pub,
                'bahasa' => 'en',
                'link' => $link->href,
                'created_at' => Carbon\Carbon::now(),

            ]
        );






        $authors = json_decode(str_replace("'", '"', $obj->author)); //formated wrond json
        // dd( $arr);

        foreach ($authors as $author) {

            $penulis =  App\Penulis::updateOrCreate(
                ['nama' => $author->name]
            );

            $exists = $karya->penulis->contains($penulis->id);
            if(!$exists)
            $karya->penulis()->attach($penulis->id);
        }




        $search = ["'", "None"];
        $replace = ['"', '""'];

        // dd($item->href);

        $arr = json_decode(str_replace($search, $replace, $obj->tag));

        echo $i;


        foreach ($arr as $temp) {
            
            $subjCode = substr($arr[1]->term, strpos($arr[0]->term, ".") + 1);

            $pembahasan = App\Pembahasan::updateOrCreate(
                [
                    'subject_code' => $subjCode
                ]
            );

            $pembahasan->createPembahasanFromArxivSubject();

            $exists = $karya->pembahasan->contains($pembahasan->id);
            // dump($exists);

            if(!$exists)
            $karya->pembahasan()->attach($pembahasan->id);

        }

        if ($i++ > $lim) break;
    }


});