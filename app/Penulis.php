<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penulis extends Model
{
    //

    public $timestamps = false;

    
    protected $table = "penulis";
    protected $fillable = [
        'nama',
    ];
    

}
