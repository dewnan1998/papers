<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTfIdfTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tf_idf';

    /**
     * Run the migrations.
     * @table tf_idf
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('freq')->nullable();
            $table->float('tf-idf')->nullable();
            $table->float('df')->nullable();
            $table->float('tf')->nullable();
            $table->unsignedInteger('token_id');
            $table->unsignedInteger('karya_ilmiah_id');

            $table->index(["token_id"], 'fk_tf-idf_token1_idx');

            $table->index(["karya_ilmiah_id"], 'fk_tf-idf_karya_ilmiah1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('token_id', 'fk_tf-idf_token1_idx')
                ->references('id')->on('token')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('karya_ilmiah_id', 'fk_tf-idf_karya_ilmiah1_idx')
                ->references('id')->on('karya_ilmiah')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
