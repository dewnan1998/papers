<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user';

    /**
     * Run the migrations.
     * @table user
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email', 45)->nullable();
            $table->string('password', 45)->nullable();
            $table->string('ip', 20)->nullable();
            $table->string('npm', 30)->nullable();
            $table->string('nama', 100)->nullable();

            $table->string('remember_token', 100)->nullable();



            $table->tinyInteger('is_registered')->nullable();
            $table->string('session', 45)->nullable();
            $table->string('username', 45)->nullable();
            $table->dateTime('created_at')->nullable();

            $table->unique(["id"], 'id_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
