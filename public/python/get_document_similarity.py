from database import getKaryaIlmiah 
# from preprocessing import tokenize
from  karya_ilmiah import KaryaIlmiah
# import numpy as np
from gensim.models import Word2Vec
from pathlib import Path
import dill
import json

import sys


m4 = Word2Vec.load('word2vec-41000-stem.model')


data = getKaryaIlmiah()

karya_ilmiah = []
for item in data :

    karya = KaryaIlmiah(item)
    karya_ilmiah.append(karya)


from DocSim import DocSim
ds = DocSim(m4)


search = str(sys.argv[1])

sim_result = ds.search(search, karya_ilmiah[0:2000])


    
res = []

for data in sim_result[1:5]:
    res.append({
        'id' : data['id'],
        'uid' : data['uid'],
        'judul' : data['judul'],
        'doc' : data['doc'],

    })
    
python2json = json.dumps(res)
print( python2json)
