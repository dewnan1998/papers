<?php

use App\KaryaIlmiah;
use App\Pembahasan;
use App\Penulis;

use Illuminate\Database\Seeder;

class DatasetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(public_path('datasets/arxivData.json'));




    $data = json_decode($jsonString, false);


    $lim = 2000;
    $i = 1;
    


    echo count($data);
    // return;


    foreach ($data as $obj) {


        // dd($obj);



        if ($i++ > $lim) break;

//    if ($i < 35375) continue;

     echo $i.'.';


        $date = new DateTime();

        $pub = $date->setDate($obj->year, $obj->month, $obj->day);



        $links = json_decode(str_replace("'", '"', $obj->link)); //format


        $link = null;
        foreach ($links as $temp) {
            if ('application/pdf' == $temp->type) {
                $link = $temp;
                break;
            }
        }






        $karya =  KaryaIlmiah::updateOrCreate(
            [
                'uid' => $obj->id,
            ],
            [
                'judul' => $obj->title,
                'summary' => $obj->summary,
                'publish' => $pub,
                'bahasa' => 'en',
                'link' => $link->href,
                'created_at' => Carbon\Carbon::now(),

            ]
        );






        $authors = json_decode(str_replace("'", '"', $obj->author)); //formated wrond json
        // dd( $arr);

        // echo var_dump($authors);
        if(!empty($authors))
        {
            foreach ($authors as $author) {

                $penulis =  Penulis::updateOrCreate(
                    ['nama' => $author->name]
                );
    
                $exists = $karya->penulis->contains($penulis->id);
                if(!$exists)
                $karya->penulis()->attach($penulis->id);
            }
    
        }
    



        $search = ["'", "None"];
        $replace = ['"', '""'];

        // dd($item->href);

        $arr = json_decode(str_replace($search, $replace, $obj->tag));



        foreach ($arr as $temp) {
            
            $subjCode = substr($temp->term, strpos($temp->term, ".") + 1);

            $pembahasan = Pembahasan::updateOrCreate(
                [
                    'subject_code' => $subjCode
                ]
            );

            $pembahasan->createPembahasanFromArxivSubject();

            $exists = $karya->pembahasan->contains($pembahasan->id);
            // dump($exists);

            if(!$exists)
            $karya->pembahasan()->attach($pembahasan->id);

        }

     
    }
    }
}
